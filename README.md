# That is a simple JAVA (Spring Boot) projet for text classification

## How it works?

> Clone the project: ` git clone https://gitlab.com/kenany/textclassifier.git `

> Run the project (indide project directory)

1. On Linux or MacOS: `./mvnw spring-boot:run`
2. On windows: `./mvnw.bat spring-boot:run`

> Build the Java Archive (.jar) executable

1. On Linux or MacOS: `./mvnw install`
2. On Windows: `./mvnw.bat install`

That should create a new directory (target). Inside the target directory, you can see a file **testclassifier-0.0.1-SNAPSHOT.jar**

> Run the jar file

``` java -jar testclassifier-0.0.1-SNAPSHOT.jar ```
