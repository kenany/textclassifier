package com.camaroes.testclassifier.classifiers;

public enum Category {

    ILLICITE("illicite", "illicite"),
    NON_ILLICITE("non illicite", "non_illicite"),
    FRENCH("Français", "fr"),
    ENGLISH("English", "en"),
    ILLICITE_INTERNET("Contenu illicite issu d'internet", "illicite_internet"),
    ILLICITE_NON_INTERNET("Contenu illicite non issu d'internet", "illicite_non_internet");
//    DEUTCH("Deutch");

    private final String name;
    private final String key;

    Category(String category, String key) {
        this.name = category;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public static Category fromKey(String key) {

        Category[] categories = Category.values();

        for (Category cat: categories) {

            if (cat.getKey().equals(key)) {
                return cat;
            }
        }

        return null;
    }
}
