/*
 * Copyright (C) 2020 kenany
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.camaroes.testclassifier.classifiers;

import java.io.Serializable;

/**
 *
 * @author kenany
 */
public class Score implements Serializable {
    
    private String category;
    
    private Double accuracy;

    public Score(String category, Double accuracy) {
        this.category = category;
        this.accuracy = accuracy;
    }
    
    

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public String toString() {
        return "Score{" +
                "category='" + category + '\'' +
                ", accuracy=" + accuracy +
                '}';
    }
}
