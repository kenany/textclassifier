package com.camaroes.testclassifier.classifiers;

import java.io.Serializable;

public class TrainModel implements Serializable {

    private Category category;

    private String text;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
