package com.camaroes.testclassifier.classifiers;

import java.io.Serializable;

public class PredictModel implements Serializable {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
