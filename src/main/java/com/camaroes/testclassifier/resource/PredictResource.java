package com.camaroes.testclassifier.resource;

import com.camaroes.testclassifier.classifiers.PredictModel;
import com.camaroes.testclassifier.classifiers.Score;
import com.camaroes.testclassifier.service.PredictService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Map;

@RestController
@RequestMapping("/api/predict")
public class PredictResource {

    private final PredictService predictService;

    public PredictResource(PredictService predictService) {
        this.predictService = predictService;
    }

    @GetMapping("/")
    public String predict() {
        return "Hello It works";
    }

    @PostMapping("/text")
    public ResponseEntity<Score> predict(@RequestBody PredictModel params) {

        String text = params.getText();

        Score score = this.predictService.predict(text);

        return ResponseEntity.ok(score);
    }

    @PostMapping("/file")
    public ResponseEntity<Score> predict(@RequestParam("file") MultipartFile file) throws IOException {

        InputStream inputStream = file.getInputStream();

        Score score = this.predictService.predict(inputStream);

        return ResponseEntity.ok(score);
    }
}
