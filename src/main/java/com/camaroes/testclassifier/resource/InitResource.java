package com.camaroes.testclassifier.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InitResource {

    @GetMapping("/")
    public String init() {
        return "Hellow!";
    }
}
