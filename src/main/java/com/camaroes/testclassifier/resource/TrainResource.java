package com.camaroes.testclassifier.resource;

import com.camaroes.testclassifier.classifiers.Category;
import com.camaroes.testclassifier.classifiers.TrainModel;
import com.camaroes.testclassifier.service.TrainService;
import com.dropbox.core.DbxException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/train")
public class TrainResource {

    private final TrainService trainService;

    public TrainResource(TrainService trainService) {
        this.trainService = trainService;
    }

    @PostMapping("/text")
    public ResponseEntity<String> train(@RequestBody TrainModel model) throws IOException, DbxException {

        this.trainService.train(model.getCategory(), model.getText());

        return ResponseEntity.ok("The Bot was successfully trained!");
    }

    @PostMapping("/file")
    public ResponseEntity<String> train(@RequestParam("category") Category category, @RequestParam("file") MultipartFile file) throws IOException, DbxException {

        this.trainService.train(category, file.getInputStream());

        return ResponseEntity.ok("The Bot was successfully trained!");
    }
}
