package com.camaroes.testclassifier.config;

import com.camaroes.testclassifier.classifiers.Category;
import com.camaroes.testclassifier.classifiers.NaiveBayes;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.users.FullAccount;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Value;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.FileList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.util.*;

@Configuration
public class ClassifierConfiguration {

//    private File resource = ResourceUtils.getFile("classpath:credentials.json");

    private Map<String, Category> map = new HashMap<>();

    private static final String APPLICATION_NAME = "classifier";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "classifier";
    private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE_METADATA_READONLY);
    private static final String CREDENTIALS_FILE_PATH = "credentials.json";

    private static final String ACCESS_TOKEN = "47MAC-GOVXQAAAAAAAAAAR4xCVdYgzQadBuHaag7nd7zrIX6lNoQgPbsxcO11mJt";

    public ClassifierConfiguration() throws FileNotFoundException {
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(100000);
        return multipartResolver;
    }

    @Bean(name = "nbalgo")
    public NaiveBayes getNayveBayesAlgorithm() throws GeneralSecurityException, IOException, DbxException {

        System.out.println("***************** Start training ************************* ");

        Map<String, Double> categoryPrios = new HashMap<>();
        categoryPrios.put(Category.ILLICITE_NON_INTERNET.getName(), 1d);
        categoryPrios.put(Category.ILLICITE_INTERNET.getName(), 0d);

        NaiveBayes nb = new NaiveBayes();
        nb.setChisquareCriticalValue(6.63);
//        nb.train(trainingXamples());

        Map<String, String[]> datas = loadDatasetsFromDropbox();

//        System.out.println("datas size: " + datas.keySet());

        nb.train(datas);

        return nb;
    }

    public Map<String, String[]> trainingXamples() throws IOException {

        System.out.println("*************** LOADING TRAINING FILES *****************");

        Map<String, URL> trainingFiles = new HashMap<>();

        Map<String, String[]> trainingExamples = new HashMap<>();

        URL url = new ClassPathResource("datasets").getURL();
        String dataSetsPath = url.getPath();

        System.out.println("**************** DATASETS PATH:  " + dataSetsPath);

        File baserDir = new File(dataSetsPath);

        System.out.println("**************** DATASETS DIRECTORY EXISTS:  " + baserDir.exists());

        if (baserDir == null) {
            throw new IOException("The datasets directory is not found!");
        }

        String[] directories = baserDir.list();

        for (String directory : directories) {

            System.out.println("******************** category:  " + directory);

            Category category = Category.fromKey(directory);

            File[] files = new File(dataSetsPath + "/" + directory).listFiles();

            List<String> lines = new ArrayList<>();

            for (File file: files) {
                lines.addAll(readLines(file));
            }

            trainingExamples.put(category.getName(), lines.toArray(new String[lines.size()]));
        }

//        ClassPathResource frenchResource = new ClassPathResource("datasets/fr/training.language.fr.txt");
//        ClassPathResource englishResource = new ClassPathResource("datasets/en/training.language.en.txt");
//        ClassPathResource deutchResource = new ClassPathResource("de/training.language.de.txt");
//
//        trainingFiles.put("Français", frenchResource.getURL());
//        trainingFiles.put("English", englishResource.getURL());
//        trainingFiles.put("Deutch", deutchResource.getURL());

//        Map<String, String[]> trainingExamples = new HashMap<>();

//        for(Map.Entry<String, URL> entry : trainingFiles.entrySet()) {
//            trainingExamples.put(entry.getKey(), readLines(entry.getValue()));
//        }

        return trainingExamples;
    }

    public static String[] readLines(URL url) throws IOException {

//        Reader fileReader = new InputStreamReader(url.openStream(), Charset.forName("UTF-8"));
        Reader fileReader = new InputStreamReader(url.openStream());
        List<String> lines;
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            lines = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines.toArray(new String[lines.size()]);
    }

    public static List<String> readLines(File file) throws IOException {

//        Reader fileReader = new FileReader(file, Charset.forName("UTF-8"));
        Reader fileReader = new FileReader(file);
        List<String> lines;
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            lines = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines;
    }

    private Credential getCredentials(NetHttpTransport httpTransport) throws IOException {

        ClassPathResource resource = new ClassPathResource(CREDENTIALS_FILE_PATH);

        Reader reader = new InputStreamReader(resource.getInputStream());

        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, reader);

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                httpTransport, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();

        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setHost("localhost").setPort(8080).setCallbackPath("/classifier/").build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    @Bean
    public DbxClientV2 getClient() {
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/datasets").build();
        return new DbxClientV2(config, ACCESS_TOKEN);
    }

    private Map<String, String[]> loadDatasetsFromDropbox() throws DbxException, IOException {

        String folderBasePath = "/datasets";

        DbxClientV2 client = getClient();

        FullAccount account = client.users().getCurrentAccount();
        System.out.println("Dropbox account: " + account.getName().getDisplayName());

        ListFolderResult result = client.files().listFolder(folderBasePath);

        List<File> files = new ArrayList<>();

        Map<String, String[]> datasets = new HashMap<>();

        for (Metadata metadata : result.getEntries()) {

            String folder = metadata.getName();
            String currentPath = metadata.getPathLower();

            ListFolderResult datas = client.files().listFolder(currentPath);

            List<String> lines = new ArrayList<>();

            String category = folder;

            for (Metadata metadata1 : datas.getEntries()) {

                String path = "datas.txt";

                OutputStream outputStream = new FileOutputStream(path);

                client.files().downloadBuilder(metadata1.getPathLower()).download(outputStream);

                File file = new File(path);

                lines.addAll(readLines(file));

                files.add(file);

//                System.out.println("************* file exists:  " + file.exists());
//                System.out.println("************* folder name:  " + metadata1.getName());
            }

            if (!lines.isEmpty()) {
                datasets.put(Category.fromKey(category).getName(), lines.toArray(new String[lines.size()]));
            }
        }

        return datasets;
    }

//    private void getFilesFromGoogleDrive() {
//
//        NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
//        Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
//                .setApplicationName(APPLICATION_NAME)
//                .build();
//
//        // Print the names and IDs for up to 10 files.
//        FileList result = service.files().list()
//                .setQ("name='classifier'")
//                .setFields("nextPageToken, files(id, name, kind, mimeType)")
//                .execute();
//        List<com.google.api.services.drive.model.File> files = result.getFiles();
//        if (files == null || files.isEmpty()) {
//            System.out.println("No files found.");
//        } else {
//
//            com.google.api.services.drive.model.File datasetsFolder = files.get(0);
//
//            FileList loaded = service.files().list()
//                    .setQ("'" + datasetsFolder.getId() + "' in parents")
//                    .setQ("mimeType='text/plain'")
//                    .setFields("nextPageToken, files(id, name, kind, mimeType, parents)")
//                    .execute();
//            List<com.google.api.services.drive.model.File> datasets = loaded.getFiles();
//
//            FileList loadedFolders = service.files().list()
//                    .setQ("'" + datasetsFolder.getId() + "' in parents")
//                    .setQ("mimeType = 'application/vnd.google-apps.folder'")
//                    .setFields("nextPageToken, files(id, name)")
//                    .execute();
//
//            List<com.google.api.services.drive.model.File> folders = loadedFolders.getFiles();
//
//            if (datasets == null || datasets.isEmpty()) {
//                System.out.println("No files found.");
//            } else {
//
//                for (com.google.api.services.drive.model.File file : datasets) {
//                    com.google.api.services.drive.model.File parent = folders.stream()
//                            .filter(f -> f.getId().equals(file.getParents()
//                                    .get(0)))
//                            .findFirst()
//                            .get();
//
//                    System.out.println("file id: " + file.getId() + "; file name: " + file.getName() + "; parent name:  " + parent.getName());
//
//                    OutputStream ou = new ByteArrayOutputStream();
//
//                    service.files().get(file.getId()).executeMediaAndDownloadTo(ou);
//
//
//
//                }
//            }
//
////            com.google.api.services.drive.model.File classifierFolder = files.get(0);
////            String folderId = classifierFolder.getId();
////
////            com.google.api.services.drive.model.File toCreate = new com.google.api.services.drive.model.File();
////            toCreate.setName(""+new Date().getTime()+".txt");
////            toCreate.setParents(Collections.singletonList(folderId));
////
////            FileContent fileContent = new FileContent("text/plain", resource);
////
////            com.google.api.services.drive.model.File created = service.files()
////                                                                        .create(toCreate)
////                                                                        .setFields("id, parents, name")
////                                                                        .execute();
////
////            System.out.println("new file name: " + created.getName());
////            System.out.println("new file parent: " + created.getParents().get(0));
////            System.out.println("new file id: " + created.getId());
//        }
//    }
}

