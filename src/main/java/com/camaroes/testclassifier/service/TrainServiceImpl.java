package com.camaroes.testclassifier.service;

import com.camaroes.testclassifier.classifiers.Category;
import com.camaroes.testclassifier.classifiers.NaiveBayes;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.users.FullAccount;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

@Service
public class TrainServiceImpl implements TrainService {

    private final NaiveBayes naiveBayes;

    private final DbxClientV2 client;

    private final String dataSetsPath;

    public TrainServiceImpl(NaiveBayes naiveBayes, DbxClientV2 client) throws IOException {
        this.naiveBayes = naiveBayes;
        this.client = client;

        URL url = new ClassPathResource("datasets").getURL();
        this.dataSetsPath = url.getPath();

    }

    @Override
    public void train(Category category, String text) throws IOException, DbxException {

        File file = new File("send.text");

        OutputStream ou = new FileOutputStream(file);

        ou.write(text.getBytes());

        ou.close();

        InputStream in = new FileInputStream(file);

        upload(in, category);

        trainBoot();

//        Map<String, String[]> trainingData = new HashMap<>();
//        trainingData.put(category.getName(), new String[]{text});
//        trainingData.put(category.getName(), new String[]{text});
//
//        String[] directories = new File(this.dataSetsPath).list();
//
//        for (String directory: directories) {
//
//            Category current = Category.fromKey(directory);
//
//            File[] files = new File(this.dataSetsPath + "/" + directory).listFiles();
//
//            List<String> lines = new ArrayList<>();
//
//            for (File file : files) {
//                lines.addAll(readLines(file));
//            }
//
//            if (current.equals(category)) {
//                lines.add(text);
//            }
//
//            String[] datas = lines.toArray(new String[lines.size()]);
//
//            trainingData.put(current.getName(), datas);
//        }
//
//        this.naiveBayes.train(trainingData);
    }

    @Override
    public void train(Category category, InputStream in) throws IOException, DbxException {

        upload(in, category);

        trainBoot();

//        InputStreamReader reader = new InputStreamReader(in);
//
//        Map<String, String[]> trainingData = new HashMap<>();
//
//        List<String> newLines = new ArrayList<>();
//
//        BufferedReader bufferedReader = new BufferedReader(reader);
//
//        String line;
//
//        while ((line = bufferedReader.readLine()) != null) {
//            newLines.add(line);
//        }
//
////        String[] datas = newLines.toArray(new String[newLines.size()]);
////
////        trainingData.put(category.getName(), datas);

//        String[] directories = new File(this.dataSetsPath).list();
//
//        for (String directory: directories) {
//
//            Category current = Category.fromKey(directory);
//
//            File[] files = new File(this.dataSetsPath + "/" + directory).listFiles();
//
//            List<String> lines = new ArrayList<>();
//
//            for (File file : files) {
//                lines.addAll(readLines(file));
//            }
//
//            if (current.equals(category)) {
//                lines.addAll(newLines);
//            }
//
//            String[] datas = lines.toArray(new String[lines.size()]);
//
//            trainingData.put(current.getName(), datas);
//        }
//
////        String[] datas = lines.toArray(new String[lines.size()]);
////
////        Map<String, String[]> trainingData = new HashMap<>();
////
////        trainingData.put(category.getName(), datas);
//
//        this.naiveBayes.train(trainingData);
    }

    private void upload(InputStream in, Category category) throws IOException, DbxException {

        String fileName = "" + new Date().getTime() + ".txt";

        this.client.files()
                .uploadBuilder("/datasets/" + category.getKey() + "/" + fileName)
                .uploadAndFinish(in);
    }

    private List<String> readLines(File file) throws IOException {

//        Reader fileReader = new FileReader(file, Charset.forName("UTF-8"));
        Reader fileReader = new FileReader(file);
        List<String> lines;
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            lines = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines;
    }

    private void trainBoot() throws IOException, DbxException {

        Map<String, Double> categoryPrios = new HashMap<>();
        categoryPrios.put(Category.ILLICITE_NON_INTERNET.getName(), 1d);
        categoryPrios.put(Category.ILLICITE_INTERNET.getName(), 0d);

        Map<String, String[]> datas = loadDatasetsFromDropbox();

        this.naiveBayes.train(datas);
    }

    private Map<String, String[]> loadDatasetsFromDropbox() throws DbxException, IOException {

        String folderBasePath = "/datasets";

        ListFolderResult result = this.client.files().listFolder(folderBasePath);

        List<File> files = new ArrayList<>();

        Map<String, String[]> datasets = new HashMap<>();

        for (Metadata metadata : result.getEntries()) {

            String folder = metadata.getName();
            String currentPath = metadata.getPathLower();

            ListFolderResult datas = this.client.files().listFolder(currentPath);

            List<String> lines = new ArrayList<>();

            String category = folder;

            for (Metadata metadata1 : datas.getEntries()) {

                String path = "datas.txt";

                OutputStream outputStream = new FileOutputStream(path);

                client.files().downloadBuilder(metadata1.getPathLower()).download(outputStream);

                File file = new File(path);

                lines.addAll(readLines(file));

                files.add(file);
            }

            if (!lines.isEmpty()) {
                datasets.put(Category.fromKey(category).getName(), lines.toArray(new String[lines.size()]));
            }
        }

        return datasets;
    }
}
