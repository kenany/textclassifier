package com.camaroes.testclassifier.service;

import com.camaroes.testclassifier.classifiers.Category;
import com.dropbox.core.DbxException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface TrainService {

    void train(Category category, String text) throws IOException, DbxException;

    void train(Category category, InputStream in) throws IOException, DbxException;
}
