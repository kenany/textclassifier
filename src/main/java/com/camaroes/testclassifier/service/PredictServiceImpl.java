package com.camaroes.testclassifier.service;

import com.camaroes.testclassifier.classifiers.NaiveBayes;
import com.camaroes.testclassifier.classifiers.Score;
import com.camaroes.testclassifier.dataobjects.NaiveBayesKnowledgeBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PredictServiceImpl implements PredictService {

    private NaiveBayes naiveBayes;

    public PredictServiceImpl(NaiveBayes naiveBayes) {
        this.naiveBayes = naiveBayes;
        Score score = this.naiveBayes.predictScore("Bonjour à tous, je me suis fait arnaqué sur whatsapp");

        System.out.println(score);
    }

    @Override
    public Score predict(String text) {
        return this.naiveBayes.predictScore(text);
    }

    @Override
    public Score predict(InputStream inputStream) throws IOException {

        InputStreamReader reader = new InputStreamReader(inputStream);

        StringBuilder builder = new StringBuilder();

        BufferedReader bufferedReader = new BufferedReader(reader);

        String line;

        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }

        return this.naiveBayes.predictScore(builder.toString());
    }

//    @PostConstruct
//    private void init() throws IOException {
//        ClassPathResource frenchResource = new ClassPathResource("datasets/training.language.fr.txt");
//
//        URL url = frenchResource.getURL();
//
//        String[] lines = readLines(url);
//
//        Map<String, String[]> training = new HashMap<>();
//
//        training.put("Français", lines);
//
//        NaiveBayesKnowledgeBase knowledgeBase = this.naiveBayes.getKnowledgeBase();
//
////        this.naiveBayes = new NaiveBayes(knowledgeBase);
//
//        this.naiveBayes.train(training);
//
////        System.out.println(naiveBayes.getKnowledgeBase().logLikelihoods.keySet());
//
//        Score score = this.naiveBayes.predictScore("Bonjour je suis Armel");
//
//        System.out.println(score.toString());
//
//        Score score2 = this.naiveBayes.predictScore("Hello I am Armel");
//
//        System.out.println(score2.toString());
//    }

    private static String[] readLines(URL url) throws IOException {

        Reader fileReader = new InputStreamReader(url.openStream(), Charset.forName("UTF-8"));
        List<String> lines;
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            lines = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines.toArray(new String[lines.size()]);
    }
}
