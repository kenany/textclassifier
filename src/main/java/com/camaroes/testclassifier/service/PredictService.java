package com.camaroes.testclassifier.service;

import com.camaroes.testclassifier.classifiers.Score;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public interface PredictService {

    Score predict(String text);

    Score predict(InputStream inputStream) throws IOException;
}
