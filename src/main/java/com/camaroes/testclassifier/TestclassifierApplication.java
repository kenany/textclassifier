package com.camaroes.testclassifier;

import com.camaroes.testclassifier.config.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SwaggerConfiguration.class)
public class TestclassifierApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestclassifierApplication.class, args);
	}

}
